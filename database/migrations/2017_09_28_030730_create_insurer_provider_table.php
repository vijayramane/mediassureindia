<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurerProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_insurer_provider', function (Blueprint $table) {
            $table->unsignedInteger('provider_id');
            $table->unsignedInteger('insurer_id');
            
            $table->foreign('provider_id')->references('id')->on('a_providers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('insurer_id')->references('id')->on('a_insurers')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['provider_id', 'insurer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_insurer_provider');
    }
}
