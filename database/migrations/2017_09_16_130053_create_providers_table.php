<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('center_name');
            $table->string('address');
            $table->string('location')->nullable();
            $table->string('city');
            $table->string('state');
            $table->integer('pincode')->nullable();
            $table->string('landmark')->nullable();
            $table->string('email');
            $table->string('tel_no')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('working_hours')->nullable();
            $table->string('sunday_hours')->nullable();
            $table->string('contact_person1')->nullable();
            $table->string('contact_person2')->nullable();
            $table->string('dc_owner')->nullable();
            $table->string('registration_no')->nullable();
            $table->string('pan_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_providers');
    }
}
