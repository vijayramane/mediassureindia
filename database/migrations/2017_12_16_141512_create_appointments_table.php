<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->string('time')->nullable();
            $table->string('application_no');
            $table->string('policy_no')->nullable();
            $table->integer('sum_assured')->nullable();
            $table->string('paid_sponsored')->nullable();
            $table->string('group_individual')->nullable();
            $table->string('visit_type')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
