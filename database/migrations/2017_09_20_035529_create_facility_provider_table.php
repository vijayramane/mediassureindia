<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilityProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_provider_facility', function (Blueprint $table) {
            $table->unsignedInteger('provider_id');
            $table->unsignedInteger('facility_id');
            
            $table->foreign('provider_id')->references('id')->on('a_providers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('facility_id')->references('id')->on('a_facilities')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['provider_id', 'facility_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_provider_facility');
    }
}
