<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_insurer_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurer_id');
            $table->string('category');
            $table->string('test_details');
            $table->integer('base_rate');
            $table->integer('our_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_insurer_packages');
    }
}
