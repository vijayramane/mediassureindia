<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentAgentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appt_agent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appointment_id');
            $table->string('agent_name')->nullable();
            $table->string('agent_code')->nullable();
            $table->string('agent_email')->nullable();
            $table->string('agent_mobile')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appt_agent');
    }
}
