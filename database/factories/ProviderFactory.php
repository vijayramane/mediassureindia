<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Provider::class, function (Faker $faker) {

    return [
        'center_name' => $faker->company,
        'address' => $faker->address,
        'location' => $faker->streetName,
        'city' => $faker->city,
        'state' => $faker->state,
        'tel_no' => $faker->phoneNumber,
        'mobile_no' => $faker->e164PhoneNumber,
        'email' => $faker->unique()->safeEmail,
    ];
});
