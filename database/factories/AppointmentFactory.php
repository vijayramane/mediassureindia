<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Appointment::class, function (Faker $faker) {
    return [
        'date' => $faker->date($format = 'Y-m-d', $max = '+2 months'),
        'time' => $faker->time($format = 'H:i:s', $max = 'now'),
        'application_no' => $faker->numberBetween($min = 1000, $max = 9000),
        'policy_no' => $faker->numberBetween($min = 1000000, $max = 9999999),
        'sum_assured' => $faker->numberBetween($min = 100, $max = 1000),
        'paid_sponsored' => $faker->randomElement($array = array ('paid','sponsored')),
        'group_individual' => $faker->randomElement($array = array ('group','individual')),
        'visit_type' => $faker->randomElement($array = array ('home','center')),
        'status_id' => $faker->numberBetween($min = 1, $max = 8)
    ];
});
