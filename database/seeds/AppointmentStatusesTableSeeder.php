<?php

use Illuminate\Database\Seeder;
use App\Models\AppointmentStatus;
use Illuminate\Support\Str;

class AppointmentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $appointment_statuses = array(
            ['name'=>'Incomplete Request', 'color'=>'#ff2600'],
            ['name'=>'New Appointment Request', 'color'=>'#942192'],
            ['name'=>'Confirmed', 'color'=>'#00f900'],
            ['name'=>'Re-Scheduled Appointment', 'color'=>'#00fdff'],
            ['name'=>'Report Pending', 'color'=>'#aa7942'],
            ['name'=>'No Show', 'color'=>'#ff9300'],
            ['name'=>'Partial Show', 'color'=>'#fffb00'],
            ['name'=>'Report Upload', 'color'=>'#ff40ff'],
        );

        foreach($appointment_statuses as $value)
        {
            $appointment_status = new AppointmentStatus;
            $appointment_status->name = $value['name'];
            $appointment_status->slug = Str::slug($value['name'], '-');
            $appointment_status->color = $value['color'];
            $appointment_status->save();
        }
    }
}
