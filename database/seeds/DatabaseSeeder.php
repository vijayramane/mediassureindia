<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);
        $this->call(FacilitiesTableSeeder::class);
        $this->call(ProvidersTableSeeder::class);
        $this->call(InsurersTableSeeder::class);
        $this->call(AppointmentStatusesTableSeeder::class);
        $this->call(CallStatusesTableSeeder::class);
        $this->call(AppointmentsTableSeeder::class);
    }
}
