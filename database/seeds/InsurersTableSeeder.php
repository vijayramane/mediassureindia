<?php

use Illuminate\Database\Seeder;

class InsurersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Insurer::class, 5)->create();
    }
}
