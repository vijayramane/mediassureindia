<?php

use Illuminate\Database\Seeder;
use App\Models\Facility;

class FacilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facilities = array('Doctors Consultation','Laboratory','ECG','X-Ray','Ultrasound','TMT','Female ECG Technician Availability','Mammography','Echo','Home Visit Facility','Doctors available for Home visit Male/Female','Technician available for Home visit Male/Female','Fax Facility','Scanning Facility');

        foreach($facilities as $value)
        {
            $facility = new Facility;
            $facility->name = $value;
            $facility->save();
        }
        
    }
}
