<?php

use Illuminate\Database\Seeder;
use App\Models\CallStatus;
use Illuminate\Support\Str;

class CallStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $call_statuses = array('Customer has requested MAHS to call back', 'Customer will call later', 'Dis-connecting', 'Engaged', 'Invalid number', 'No home visit & Center visit facility', 'Not reachable', 'Out of coverage area', 'Policy cancelled', 'Reachable but not responding', 'Responding but not interested', 'Switched off', 'Wrong Number', 'MAHS initiated call later', 'Client is not interested', 'Advisor not aware of the medicals', 'Cancelled by client');

        foreach($call_statuses as $value)
        {
            $call_status = new CallStatus;
            $call_status->name = $value;
            $call_status->slug = Str::slug($value, '-');
            $call_status->save();
        }
    }
}
