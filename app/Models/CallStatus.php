<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CallStatus extends Model
{
    protected $table = 'a_call_statuses';
    use SoftDeletes;
}
