<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentAgent extends Model
{

    protected $table = 'appt_agent';

    protected $fillable = [
        'agent_name', 'agent_code', 'agent_email', 'agent_mobile',
    ];
}
