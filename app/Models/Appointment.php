<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{

    protected $table = 'appointments';

    protected $fillable = [
        'display_id', 'date', 'time', 'application_no', 'policy_no', 'sum_assured', 'paid_sponsored', 'group_individual', 'visit_type', 'status'
    ];


    public function customer()
    {
        return $this->hasOne('App\Models\AppointmentCustomer', 'appointment_id');
    }

    public function agent()
    {
        return $this->hasOne('App\Models\AppointmentAgent', 'appointment_id');
    }

    public function insurer()
    {
        return $this->hasOne('App\Models\AppointmentInsurer', 'appointment_id');
    }

    public function packages()
    {
        return $this->belongsToMany('App\Models\Package', 'appt_packages');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\AppointmentStatus', 'status_id');
    }

}
