<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Insurer extends Model
{
    protected $table = 'a_insurers';

    protected $fillable = ['name', 'address', 'contact_person', 'contact_no'];

    public function packages()
    {
        return $this->hasMany('App\Models\Package');
    }

    public function providers()
    {
        return $this->belongsToMany('App\Models\Provider', 'a_insurer_providers');
    }

    public function divisions()
    {
        return $this->hasMany('App\Models\InsurerDivision');
    }
}
