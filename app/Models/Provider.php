<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'a_providers';

    protected $fillable = ['center_name', 'address', 'location', 'city', 'state', 'pincode', 'landmark', 'email', 'tel_no', 'mobile_no', 'fax_no', 'working_hours', 'sunday_hours', 'registration_no', 'pan_no', 'dc_owner', 'contact_person1', 'contact_person2'];

    public function facilities()
    {
        return $this->belongsToMany('App\Models\Facility', 'a_provider_facility');
    }

    public function insurers()
    {
        return $this->belongsToMany('App\Models\Insurer', 'a_insurer_provider');
    }
}
