<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentInsurer extends Model
{
    
    protected $table = 'appt_insurer';

    protected $fillable = [
        'insurer_id', 'division_id', 'branch',
    ];

    public function insurer()
    {
        return $this->belongsTo('App\Models\Insurer', 'insurer_id');
    }

    public function division()
    {
        return $this->belongsTo('App\Models\InsurerDivision', 'division_id');
    }
}
