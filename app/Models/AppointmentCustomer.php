<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentCustomer extends Model
{
    
    protected $table = 'appt_customer';

    protected $fillable = [
        'name', 'gender', 'age', 'mobile', 'email', 'address', 'state', 'city', 'pincode', 'annual_premium'
    ];
}
