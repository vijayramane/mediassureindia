<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'a_insurer_packages';

    protected $fillable = ['insurer_id', 'category', 'test_details', 'base_rate', 'our_rate'];

    public function insurer()
    {
        return $this->belongsTo('App\Insurer');
    }
}
