<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsurerDivision extends Model
{
    
    protected $table = 'a_insurer_divisions';
}
