<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $table = 'a_facilities';

    public function providers()
    {
        return $this->belongsToMany('App\Models\Provider', 'a_provider_facility');
    }
}
