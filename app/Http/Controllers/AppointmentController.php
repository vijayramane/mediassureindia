<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Insurer;
use App\Models\Appointment;
use App\Models\AppointmentLog;
use App\Models\CallStatus;
use App\Models\AppointmentStatus;
use DB;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointments = Appointment::with(['customer', 'insurer', 'packages:package_id,category'])->paginate(5);
        $appointment_statuses = AppointmentStatus::pluck('name', 'id');
        
        return view('appointment.index', compact('appointments', 'appointment_statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $insurers = Insurer::orderBy('name', 'asc')->get(); 
        return view('appointment.create', compact('insurers'));
    }

    /**
     * Get division
     *
     * @return \Illuminate\Http\Response
     */
    public function getDivision(Request $request)
    {
        $divisions = Insurer::find($request->insurer_id)->divisions()->get();
        return $divisions;
    }

    /**
     * Get Packages
     *
     * @return \Illuminate\Http\Response
     */
    public function getPackages(Request $request)
    {
        $packages = Insurer::find($request->insurer_id)->packages()->get();
        return $packages;
    }

    /**
     * Get get_insurer_providers_state
     *
     * @return \Illuminate\Http\Response
     */
    public function getInsurerProvidersState(Request $request)
    {
        $states = DB::table('m_insurer_providers as ip')
                        ->join('m_providers as p', 'ip.provider_id', '=', 'p.id')
                        ->where('ip.insurer_id', $request['insurer_id'])
                        ->distinct()
                        ->select('p.id', 'p.state as name')
                        ->get();
        return $states;
    }

    /**
     * Get City
     *
     * @return \Illuminate\Http\Response
     */
    public function getCity(Request $request)
    {
        $city = DB::table('m_insurer_providers as ip')
            ->join('m_providers as p', 'ip.provider_id', '=', 'p.id')
            ->where('ip.insurer_id', $request['insurer_id'])
            ->where('p.state', $request['state'])
            ->distinct()
            ->select('p.id', 'p.city as name')
            ->get();
        return $city;
    }

    /**
     * Get Area
     *
     * @return \Illuminate\Http\Response
     */
    public function getArea(Request $request)
    {
        $city = DB::table('m_insurer_providers as ip')
            ->join('m_providers as p', 'ip.provider_id', '=', 'p.id')
            ->where('ip.insurer_id', $request['insurer_id'])
            ->where('p.state', $request['state'])
            ->where('p.city', $request['city'])
            ->distinct()
            ->select('p.id', 'p.location as name')
            ->get();
        return $city;
    }

    /**
     * Get Providers
     *
     * @return \Illuminate\Http\Response
     */
    public function getProviders(Request $request)
    {
        $city = DB::table('m_insurer_providers as ip')
            ->join('m_providers as p', 'ip.provider_id', '=', 'p.id')
            ->where('ip.insurer_id', $request['insurer_id'])
            ->where('p.state', $request['state'])
            ->where('p.city', $request['city'])
            ->where('p.location', $request['location'])
            ->select('p.id', 'p.center_name as name')
            ->get();
        return $city;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        
        // Create Appointment
        $appointment = Appointment::updateOrCreate($request->appointment);

        // Create Customer
        $customer = $appointment->customer()->updateOrCreate($request->customer);

        // Create Agent
        $agent = $appointment->agent()->updateOrCreate($request->agent);

        // Create Customer Insurer
        $agent = $appointment->insurer()->updateOrCreate($request->customer_insurer);

        // Appointment packages
        $packages = $appointment->packages()->sync($request->packages);
        
        $message = $appointment ? "success:Appointment created successfully" : "error:Something went wrong :(";
        return redirect('appointment/'.$appointment->id)->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointment = Appointment::with('customer', 'agent', 'insurer')->where('id', $id)->first();
        $appointment_logs = AppointmentLog::where('appointment_id', $id)->get();
        $call_statuses = CallStatus::pluck('name', 'id');
        $calling_history = DB::table('d_calling_history as ch')
                            ->join('users as u', 'ch.user_id', 'u.id')
                            ->where('ch.appointment_id', $appointment->id)
                            ->select('ch.*', 'u.name')
                            ->latest()
                            ->get();
        return view('appointment.show', compact('appointment', 'appointment_logs', 'call_statuses', 'calling_history'));

        // $appointment = DB::table('d_appointments')->where('id', $id)->first();
        // $customer = DB::table('d_appointment_customer')->where('appointment_id', $id)->first();;
        // return view('appointment.show', compact('appointment', 'appointment_logs', 'customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function change_appointment_status(Request $request)
    {
        $log = new AppointmentLog;
        $log->appointment_id = $request->appointment_id;
        $log->status = $request->status;
        $log->new_data = json_encode($request->log);
        $log->comment = $request->remark;
        $log->save();

        $appointment = Appointment::find($request->appointment_id);
        $appointment->status = $request->status;
        $appointment->save();

        $message = $log ? "success:Appointment ".$request->status : "error:Something went wrong :(";
        return back()->with('message', $message);
    }

    public function storeCallingHistory(Request $request)
    {
        $id = DB::table('d_calling_history')->insertGetId([
            'appointment_id' => $request->appointment_id, 
            'user_id' => $request->user_id,
            'receiver' => $request->receiver,
            'call_status_id' => $request->call_status_id,
            'notes' => $request->notes,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        $message = $id ? "success:Calling Status Added Successfully" : "error:Something went wrong :(";
        return back()->with('message', $message);
    }

}
