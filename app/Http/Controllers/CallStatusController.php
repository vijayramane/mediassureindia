<?php

namespace App\Http\Controllers;

use App\Models\CallStatus;
use Illuminate\Http\Request;

class CallStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $call_statuses = CallStatus::paginate(8);
        return view('admin/call-statuses/index', compact('call_statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $call_status = new CallStatus;
        $call_status->name = $request->name;
        $call_status->save();

        $message = $call_status ? "success:Call status saved successfully" : "error:Something went wrong";

        return redirect('/call-status')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CallStatus $call_status)
    {
        $call_status->name = $request->name;
        $return = $call_status->save();

        $message = $return ? "success:Call status saved successfully" : "error:Call status saving error";

        return redirect('/call-status')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CallStatus $call_status)
    {
        $return = $call_status->delete();
        $message = $return ? "success:Deleted Successfully" : "error:Error Deleting Facility"; 

        return redirect('/call-status')->with('message', $message);
    }
}
