<?php

namespace App\Http\Controllers;

use App\Models\Insurer;
use App\Models\Package;
use App\Models\Provider;
use DB;
use Illuminate\Http\Request;

class InsurerController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         //
     }

     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insurers = Insurer::paginate(10);
        return view('admin.insurers.index', compact('insurers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insurer = Insurer::create($request->except('_token'));
        $message = $insurer ? 'success:Created successfully':'error:Error while creating Insurer';
        return redirect('insurer/'.$insurer->id)->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Insurer  $insurer
     * @return \Illuminate\Http\Response
     */
    public function show(Insurer $insurer)
    {
        $providers = $insurer->providers()->get();
        $divisions = $insurer->divisions()->get();
        $packages = $insurer->packages()->get();
        return view('admin.insurers.show', compact('insurer', 'packages', 'providers', 'divisions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Insurer  $insurer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Insurer $insurer)
    {
        $result = $insurer->update($request->all());
        $message = $result ? 'success:Updated Successfully' : 'error:Error !!!';
        return redirect('/insurer/'.$insurer->id)->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Insurer  $insurer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Insurer $insurer)
    {
        $insurer->providers()->delete();
        $insurer->packages()->delete();
        $insurer->divisions()->delete();
        $result = $insurer->delete();
        $message = $result ? 'success:Deleted Successfully':'error:Error while deleting';
        return redirect('insurer')->with('message', $message);
    }

    /**
     * Store a newly created rate card.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function storePackage(Request $request)
     {
         $package = Package::create($request->except('_token'));
         $message = $package ? 'success:Stored successfully':'error:Error while creating rate card';
         return redirect('insurer/'.$package->insurer_id)->with('message', $message);
     }

     /**
     * Update rate card.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function updatePackage(Request $request, Package $package_id)
     {
        $package_id->fill($request->all());
        $result = $package_id->save();
        $message = $result ? 'success:Updated successfully':'error:Error while updating rate card';
        return redirect('insurer/'.$request->insurer_id)->with('message', $message);
     }

     /**
     * Delete rate card.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function deletePackage(Package $package_id, $insurer_id)
     {
         $result = $package_id->delete();
         $message = $result ? 'success:Deleted successfully':'error:Error while deleting rate card';
         return redirect('insurer/'.$insurer_id)->with('message', $message);
     }

     /**
     * search providers.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function searchProvider(Request $request)
     {
        $insurer_providers = Insurer::find($request->insurer_id)->providers()->pluck('provider_id');
        $providers = Provider::where('center_name', 'like', '%'.$request->searchString.'%')
                ->whereNotIn('id', $insurer_providers)
                ->orWhere('id', '=', $request->searchString)
                ->paginate(5);
        return $providers;
     }

     /**
     * add provider.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function addProvider(Request $request)
     {
        $insurer = Insurer::find($request->insurer_id)->providers()->attach($request->provider_id);
        
        $message = 'success:Added Successfully';
        return redirect('insurer/'.$request->insurer_id)->with('message', $message);
     }

     /**
     * delete provider ref.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function deleteProvider($insurer_id, $provider_id)
     {
        $insurer = Insurer::find($insurer_id)->providers()->detach($provider_id);

        $message = 'success:Deleted Successfully';
        return redirect('insurer/'.$insurer_id)->with('message', $message);
     }

     /**
     * add division in insurer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function storeDivision(Request $request)
     {
        $id = DB::table('m_insurer_divisions')->insert([
            'insurer_id' => $request->insurer_id,
            'name' => $request->division,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
         ]);
        $message = $id ? 'success:Stored successfully':'error:Error while creating division';
        return redirect('insurer/'.$request->insurer_id)->with('message', $message);
     }

     /**
     * Update Division.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function updateDivision(Request $request, $division_id)
     {
        $result = DB::table('m_insurer_divisions')->where('id', $division_id)->update(['name'=>$request->division, 'updated_at'=>date('Y-m-d H:i:s')]);
        $message = $result ? 'success:Updated successfully':'error:Error while updating division';
        return redirect('insurer/'.$request->insurer_id)->with('message', $message);
     }

     /**
     * Delete Division.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function deleteDivision($division_id)
     {
        $result = DB::table('m_insurer_divisions')->where('id', $division_id)->delete();
        $message = $result ? 'success:Deleted successfully':'error:Error while deleting division';
        return back()->with('message', $message);
     }

     /**
     * Search Insurer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function searchInsurers(Request $request)
     {
        $insurers = Insurer::where('name', 'like', '%'.$request->searchString.'%')
                    ->paginate(10);
        
        return view('admin.insurers.table', compact('insurers'));
     }
}
