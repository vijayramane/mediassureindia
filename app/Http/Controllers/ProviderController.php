<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provider;
use App\Models\Facility;
use App\Models\Insurer;

class ProviderController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         //
     }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Provider::paginate(10);
        return view('admin.providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $facilities = Facility::all();
        return view('admin.providers.create', compact('facilities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provider = $request->all('provider');
        $result = Provider::create($provider['provider']);
        $result->facilities()->sync($request->facilities);
        
        $message = $result ? 'success:Saved Successfully' : 'error:Error in save';
        return redirect('/provider')->with('message', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::with(['insurers', 'facilities'])->find($id);
        $facilities = Facility::all();
        $available_facilities = $provider->facilities->pluck('id');
        
        $all_insurers = Insurer::whereNotIn('id', $provider->insurers->pluck('id'))->orderBy('name', 'asc')->get();
    
        return view('admin.providers.show', compact('provider', 'facilities', 'all_insurers', 'available_facilities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provider $provider)
    {
        $providerData = $request->all('provider');
        $result = $provider->where('id', $provider->id)->update($providerData['provider']);

        $provider->facilities()->sync($request->facilities);

        $message = $result ? 'success:Saved Successfully' : 'error:Error in save';
        return redirect('/provider/'.$provider->id)->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        $provider->delete();
        $provider->facilities()->detach();

        $message = 'success:Deleted Successfully';
        return redirect('/provider')->with('message', $message);
    }

    /**
     * Search Providers.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function searchProviders(Request $request)
     {
        $providers = Provider::where('center_name', 'like', '%'.$request->searchString.'%')
                    ->orWhere('id', '=', $request->searchString)
                    ->paginate(10);
        
        return view('admin.providers.table', compact('providers'));
     }

    /**
     * Add Insurer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addInsurer(Request $request)
    {
        $provider = Provider::find($request->provider_id);
        $provider->insurers()->attach($request->insurer_id);

        $message = 'success:Added Successfully';
        return back()->with('message', $message);
    }

    /**
     * delete provider ref.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteInsurer($provider_id, $insurer_id)
    {
        $provider = Provider::find($provider_id);
        $result = $provider->insurers()->detach($insurer_id);

        $message = $result ? 'success:Deleted Successfully' : 'error:Error !!!';
        return back()->with('message', $message);
    }

}
