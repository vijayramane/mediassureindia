<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use DB;

class UserController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    { 
        $users = User::with('roles')->paginate(10);

        $roles = Role::all();

        return view('admin.users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "Create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);

        if($user)
        {
            $role = $user->roles()->sync($request['role']);

            if($role)
            {
                $message = 'success:User Created Successfuly!';
            }else{
                $message = 'warning:Role Creation Error!';
            }

        }else{
            $message = 'error:User Creation Error'.json_encode($user);
        }

        return redirect('user')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userDetails = User::with('roles')->find($id);
        $roles = Role::all();

        return view('admin.users.show', compact('userDetails', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userDetails = User::find($id);
        $userDetails->name = $request->name;
        $userDetails->save();

        $role = DB::table('u_role_user')->where('user_id', $id)->update(['role_id' => $request->role]);

        return redirect('user/'.$id)->with('message', 'success:User details updated successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        $role = DB::table('u_role_user')->where('user_id', $id)->delete();

        return redirect('user')->with('message', 'success:User Deleted Successfully');
    }
}
