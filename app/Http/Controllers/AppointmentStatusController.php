<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AppointmentStatus;
use Illuminate\Support\Str;

class AppointmentStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointment_statuses = AppointmentStatus::paginate(8);
        return view('admin/appointment-statuses/index', compact('appointment_statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $appointment_status = new AppointmentStatus;
        $appointment_status->name = $request->name;
        $appointment_status->slug = Str::slug($request->name, '-');
        $appointment_status->color = $request->color;
        $appointment_status->save();

        $message = $appointment_status ? "success:Appointment status saved successfully" : "error:Something went wrong";

        return redirect('/appointment-status')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AppointmentStatus $appointment_status)
    {
        return view('admin/appointment-statuses/edit', compact('appointment_status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppointmentStatus $appointment_status)
    {
        $appointment_status->name = $request->name;
        $appointment_status->slug = Str::slug($request->name, '-');
        $appointment_status->color = $request->color;
        $appointment_status->save();

        $message = $appointment_status ? "success:Appointment status updated successfully" : "error:Something went wrong";

        return redirect('/appointment-status')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppointmentStatus $appointment_status)
    {
        $return = $appointment_status->delete();

        $message = $return ? "success:Deleted Successfully" : "error:Error Deleting Appointment Status"; 
        return redirect('/appointment-status')->with('message', $message);
    }
}
