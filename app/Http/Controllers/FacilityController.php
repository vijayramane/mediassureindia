<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use Illuminate\Http\Request;

class FacilityController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facilities = Facility::paginate(8);
        return view('admin.facilities.index', compact('facilities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $facility = new Facility;
        $facility->name = $request->name;
        $facility->save();

        $message = $facility ? "success:Facility saved successfully" : "error:Something went wrong";

        return redirect('/facility')->with('message', $message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility)
    {
        $facility->name = $request->name;
        $return = $facility->save();

        $message = $return ? "success:Facility saved successfully" : "error:Facility saving error";

        return redirect('/facility')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility)
    {
        $return = $facility->delete();
        $message = $return ? "success:Deleted Successfully" : "error:Error Deleting Facility"; 

        return redirect('/facility')->with('message', $message);
    }
}
