var UIToastr = function () {

    return {
        //main function to initiate the module
        init: function () {

                toastCount = 0;

                var msg = $('#message').val();
                var toastIndex = toastCount++;

                toastr.options = {
                    closeButton: true,
                };

                if (msg) {
                    msgArray = msg.split(':');
                    toastType = msgArray[0];
                    title = msgArray[0].charAt(0).toUpperCase() + msgArray[0].slice(1)+'!';
                    msg = msgArray[1];

                    var $toast = toastr[toastType](msg, title); // Wire up an event handler to a button in the toast, if it exists
                }
        }

    };

}();

jQuery(document).ready(function() {    
   UIToastr.init();
});