$(document).ready(function(){
    
	//////////////////// Added CSRF TOKEN in every ajax request ///////////////////
	$.ajaxSetup({
		headers: {
			"X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
		}
	});
	///////////////////////////// End /////////////////////////////////////////////
	searchInsurers();
	searchProviders();
	set_appointment_id();
	///////////////////////////// Manage Facilities ///////////////////////////////
	$("#add-facility").on("click", function(){
		$(".facility-modal-title").text("Add Facility");
		$("#facilityAddEdit").remove($("#edit-facility"));
		$("#facilityName").val("");
	});

	$(".edit-facility").on("click", function(){
		$("#facilityAddEdit").prop("action", "/facility/"+this.id); // change form action url
		$("#facilityAddEdit").append("<input type='hidden' id='edit-facility' name='_method' value='PUT'>"); // append form submit method
        
		$("#facilityName").val($(this).text());
	});
	///////////////////////////// End //////////////////////////////////////////////

	///////////////////////////// Manage Call Statuses ///////////////////////////////
	$("#add-call-status").on("click", function(){
		$(".call-status-modal-title").text("Add Call Status");
		$("#callStatusAddEdit").remove($("#edit-call-status"));
		$("#callStatusName").val("");
	});

	$(".edit-call-status").on("click", function(){
		$("#callStatusAddEdit").prop("action", "/call-status/"+this.id); // change form action url
		$("#callStatusAddEdit").append("<input type='hidden' id='edit-call-status' name='_method' value='PUT'>"); // append form submit method
        
		$("#callStatusName").val($(this).text());
	});
	///////////////////////////// End //////////////////////////////////////////////

	///////////////////////////// Add, Edit Rate cards for insurer /////////////////
	$(".edit-rate-card").on("click", function(){
		var id = this.className.split(" ").pop(); // clicked id number
		var PackageId = $("#rate-card-id-"+id).val(); // rate card id
        
		$("#rate-card-form").prop("action", "/insurer/update_package/"+PackageId); // change form action url
		$("#rate-card-form").append("<input type='hidden' id='editPackage' name='_method' value='PUT'>"); // append form submit method
        
		$(".rate-card-input").val(""); //Empty form

		$("#inputCategory").val($("#category-"+id).text());
		$("#inputTestDetails").val($("#test-details-"+id).text());
		$("#inputBaseRate").val($("#base-rate-"+id).text());
		$("#inputOurRate").val($("#our-rate-"+id).text());
	});

	$(".add-rate-card").on("click", function(){
		$(".rate-card-input").val(""); //Empty form
		$("#rate-card-form").prop("action", "/insurer/store_package"); // change form action url
		$("#editPackage").remove();
	});
	////////////////////////////// End ////////////////////////////////////////////

	////////////////////////////// Manage Insurer Division ////////////////////////
	$("#add-division").on("click", function(){
		$("#inputDivision").val(""); //Empty form
		$("#division-form").prop("action", "/store_division"); // change form action url
		$("#editDivision").remove();
	});

	$(".edit-division").on("click", function(){
		$("#division-form").prop("action", "/update_division/"+this.id); // change form action url
		$("#division-form").append("<input type='hidden' id='editDivision' name='_method' value='PUT'>"); // append form submit method
        
		$("#inputDivision").val($(this).text());
	});
	////////////////////////////// End ////////////////////////////////////////////

	////////////////////////////// Search Providers ///////////////////////////////
	$("#search-provider").on("keyup", function(){
		var rows = "";

		var searchString = $("#search-provider").val();
		var insurer_id = $("#insurer-id").val();

		if(searchString.length > 0) {
			$.ajax({
				type: "POST",
				url: "/search_provider",
				data: {"searchString":searchString, "insurer_id":insurer_id},
				// dataType: "json",
				success: function (data) {
					if(data["data"].length > 0) {
						for (var i = 0; i < data["data"].length; i++) {
							rows = rows+"<tr><td>"+data["data"][i]["id"]+"</td><td>"+data["data"][i]["center_name"]+"</td><td>"+data["data"][i]["location"]+"</td><td>"+data["data"][i]["state"]+"</td><td><form action=\"/add_provider\" method=\"post\"><input type=\"hidden\" name=\"_token\" value=\"" + $("meta[name=\"csrf-token\"]").attr("content") + "\" /><input type=\"hidden\" name=\"insurer_id\" value=\""+insurer_id+"\" /><input type=\"hidden\" name=\"provider_id\" value=\"" + data["data"][i]["id"] + "\" /><button type=\"submit\" class=\"btn btn-info btn-xs\">Add</button></form></td></tr>";
						}
					}else{
						rows = "<tr><td colspan=\"5\">No Data Found</td></tr>";
					}

					$("#providers-list").html(rows);
				},
				error: function (data, errorThrown) {
					// console.log(errorThrown);
					// console.log(data);
					rows = "<tr><td colspan='5'>"+errorThrown+"</td></tr>";
					$("#providers-list").html(rows);
				}
			});
		}

		$("#providers-list").html(rows);
	});
	////////////////////////////////////// End //////////////////////////////////////

	////////////////////////////// Search Insurers Datatable ///////////////////////////////
	$("#search-insurers").on("keyup", function(){
		var searchString = $("#search-insurers").val();
		searchInsurers(searchString); 
	});

	////////////////////////////// Search Providers Datatable ///////////////////////////////
	$("#search-providers").on("keyup", function(){
		var searchString = $("#search-providers").val();
		searchProviders(searchString); 
	});

	////////////////////////////// Select Insurer ///////////////////////////////
	$(".select-insurer").on("change", function(){
		var insurerId = $(this).val();
		if(insurerId) {

			$("#select-division").attr("disabled", false);
			var url = "/get_division";
			ajaxCall("GET", url, {insurer_id:insurerId}, "select-division", true);

			// $('#test-to-be-done').html('');
			// var url = "/get_packages";
			// ajaxCall('GET', url, {insurer_id:insurerId}, 'test-master');

			url = "/get_insurer_providers_state";
			ajaxCall("GET", url, {insurer_id:insurerId}, "select-state");

			$("#test-to-be-done").html("");
			var options = "";
			$.ajax({
				type: "GET",
				url: "/get_packages",
				data: {insurer_id:insurerId},
				dataType: "json",
				success: function (response) {
					// console.log(response);
					for (var i = 0; i < response.length; i++) {
						options = options+"<option value="+response[i]["id"]+" class=\"selectable\">"+response[i]["category"]+"</option>";
					}
					$("#test-master").html(options);            
				},
				error: function(data, errorThrown) {
					// console.log(errorThrown);
					options = "<option class=\"selectable\" disabled>"+errorThrown+"</option>";
					$("#test-master").html(options);
				}
			});
		} 
	});

	$(document).on("click", ".selectable", function(){
		var selectId = this.parentElement.id;
		if(selectId == "test-master") {
			$("#test-to-be-done").append(this);
			$("#test-to-be-done option").prop("selected", true);
		}

		if(selectId == "test-to-be-done") {
			$("#test-master").append(this);
			$("#test-master option").prop("selected", false);
			$("#test-to-be-done option").prop("selected", true);
		}
	});

	// select city
	$(document).on("change", "#select-state", function(){
		var insurerId = $(".select-insurer").val();
		var url = "/get_city";
		ajaxCall("GET", url, {insurer_id:insurerId, state: $(this).val()}, "select-city");
	});

	// select area
	$(document).on("change", "#select-city", function(){
		var insurerId = $(".select-insurer").val();
		var url = "/get_area";
		ajaxCall("GET", url, {insurer_id:insurerId, state: $("#select-state").val(), city: $(this).val()}, "select-area");
	});

	// select provider
	$(document).on("change", "#select-area", function(){
		var insurerId = $(".select-insurer").val();
		var url = "/get_providers";
		ajaxCall("GET", url, {insurer_id:insurerId, state: $("#select-state").val(), city: $("#select-city").val(), location: $(this).val()}, "select-providers", true);
	});


});
// end of document ready function


function searchInsurers(searchString)
{
	$.ajax({
		type: "POST",
		url: "/search_insurers",
		data: {searchString:searchString},
		success: function (data) {
			$("#insurers-table").html(data);
		},
		error: function (data, errorThrown) {
			// console.log(errorThrown);
			$("#insurers-table").html(errorThrown);
		}
	});
}

function searchProviders(searchString)
{
	$.ajax({
		type: "POST",
		url: "/search_providers",
		data: {searchString:searchString},
		success: function (data) {
			$("#providers-table").html(data);
		},
		error: function (data, errorThrown) {
			// console.log(errorThrown);
			$("#providers-table").html(errorThrown);
		}
	});
}

function set_appointment_id(id)
{
	$(".appointment_id").val(id);
	$("#date-of-visit").val($("#date-time-"+id).text());
	$("#current-requested-date").val($("#date-time-"+id).text());
}

function ajaxCall(actionType, url, params, id, setValue) {
	var options = "<option>Please Select</option>";

	$.ajax({
		type: actionType,
		url: url,
		data: params,
		dataType: "json",
		success: function (response) {
			for (var i = 0; i < response.length; i++) {
				if(setValue) {
					options = options+"<option value="+response[i]["id"]+">"+response[i]["name"]+"</option>";
				}else{
					options = options+"<option>"+response[i]["name"]+"</option>";
				}
			}
			$("#"+id).html(options);  
			// console.log(response);          
		},
		error: function(data, errorThrown) {
			// console.log(errorThrown);
			options = "<option disabled>"+errorThrown+"</option>";
			$("#"+id).html(options);
		}
	});
}