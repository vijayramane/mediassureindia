$(document).ready(function(){

	$.validator.setDefaults({
		errorElement: "span",
		errorClass: "label label-danger",
		highlight: function (element) {
			$(element).closest(".form-group").addClass("has-error");
		},
		unhighlight: function (element) {
			$(element).closest(".form-group").removeClass("has-error");
		},
		errorPlacement: function (error, element) {
			if (element.parent(".input-group").length || element.prop("type") === "checkbox" || element.prop("type") === "radio") {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});



	$("#createUser").validate({
		rules: {
			"password": {
				required: true,
				minlength: 6
			},
			"password_confirmation": {
				equalTo: "#password"
			},
		}
	});

	$("#add_provider").validate();

	$("#create-insurer").validate();
	$("#add-rate-card").validate();

	$(".delete-this").on("click", function(){
		var id_class = this.id;

		$("."+id_class).validate({
			submitHandler: function(form) {
				if (confirm("Are you sure want to DELETE ?")) {
					form.submit();
				}
			}
		});
	});

});