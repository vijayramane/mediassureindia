@extends('layouts.app')
@section('title', 'Appointment Statuses')
@section('appointment-status', 'active')
@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Appointment Statuses</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <form class="form-horizontal" method="post" action="{{ url('appointment-status/'.$appointment_status->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="appointmentStatusName" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="appointmentStatusName" value="{{ $appointment_status->name }}" name="name" placeholder="Name" required />
                            </div>
                        </div>
                        <div class="form-group">
                                <label for="appointmentStatusSlug" class="col-sm-2 control-label">Slug</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="appointmentStatusSlug" value="{{ $appointment_status->slug }}" disabled />
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="appointmentStatusColor" class="col-sm-2 control-label">Color</label>
                            <div class="col-sm-8">
                                <input type="color" class="form-control" id="appointmentStatusColor" value="{{ $appointment_status->color }}" name="color" placeholder="Color" required />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ url('appointment-status') }}" class="btn btn-default pull-left">Back</a>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection