@extends('layouts.app')
@section('title', 'Appointment Statuses')
@section('appointment-status', 'active')
@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Appointment Statuses</h3>
                <button type="button" id="add-appointment-status" class="btn btn-info pull-right" data-toggle="modal" data-target="#modal-add-edit-appointment-status"> Add </button>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Color</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($appointment_statuses as $appointment_status)
                        <tr>
                            <td>{{$loop->index+1 + ($appointment_statuses->currentPage()-1) * $appointment_statuses->perPage()}}</td>
                            <td>{{$appointment_status->name}}</td>
                            <td>{{$appointment_status->slug}}</td>
                            <td><div style="background-color:{{$appointment_status->color}};">{{$appointment_status->color}}</div></td>
                            <td>{{$appointment_status->created_at}}</td>
                            <td>{{$appointment_status->updated_at}}</td>
                            <td class="row">
                                <div class="col-md-1">
                                <a class="btn btn-xs btn-info" href="{{ url('/appointment-status/'.$appointment_status->id.'/edit') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                </div>
                                <div class="col-md-1">
                                <form class="delete-appointment-status-{{$appointment_status->id}}" action="{{'/appointment-status/'.$appointment_status->id}}" method="post" >
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" id="delete-appointment-status-{{$appointment_status->id}}" class="btn btn-danger btn-xs delete-this"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                {{$appointment_statuses->links()}}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-add-edit-appointment-status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="appointment-status-modal-title"></h4>
            </div>
            <form id="appointmentStatusAddEdit" class="form-horizontal" method="post" action="{{'/appointment-status'}}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="appointmentStatusName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="appointmentStatusName" name="name" placeholder="Name" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="appointmentStatusColor" class="col-sm-2 control-label">Color</label>
                        <div class="col-sm-8">
                            <input type="color" class="form-control" id="appointmentStatusColor" name="color" placeholder="Color" required />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection