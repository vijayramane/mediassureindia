@extends('layouts.app')
@section('title', 'Create Insurer')
@section('insurers', 'active')
@section('content')
<div class="row">
  <div class="col-md-4">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Create Insurer</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
            <label for="inputName" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputName" name="name" placeholder="Insurer Name" required >
            </div>
          </div>
          <div class="form-group">
            <label for="inputDivision" class="col-sm-2 control-label">Division</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputDivision" name="division" placeholder="Division Name" required >
            </div>
          </div>
          <div class="form-group">
            <label for="inputAddress" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-10">
              <textarea class="form-control" id="inputAddress" name="address" placeholder="Address" required ></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="inputContactPerson" class="col-sm-2 control-label">Contact Person</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputContactPerson" name="contact_person" placeholder="Contact Person Name" required >
            </div>
          </div>
          <div class="form-group">
            <label for="inputContactNo" class="col-sm-2 control-label">Contact Number</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputContactNo" name="contact_no" placeholder="Contact Number" required >
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="btn-group pull-right">
            <button type="submit" class="btn btn-info">Save</button>
            <a href="{{'/insurer'}}" class="btn btn-default">Cancel</a>
          </div>
        </div>
        <!-- /.box-footer -->
      </form>
    </div>
  </div>
  <div class="col-md-8">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Rate Card</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
        <div class="box-body">
        <input type="hidden" id="total-categories" value="">
        <table class="table table-bordered">
                <thead>
                  <tr>
                    <th width="25%">Category</th>
                    <th width="40%">Test Details</th>
                    <th width="15%">Actual Rate</th>
                    <th width="15%">Our Rate</th>
                    <th width="5%">#</th>
                  </tr>
                </thead>
                <tbody class="rate-card">
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="5">
                      <button type="button" class="btn btn-info" id="add-rate-card-box">+</button>
                    </th>
                  </tr>
                </tfoot>
                </table>
        </div>
    </div>
  </div>
</div>
@endsection