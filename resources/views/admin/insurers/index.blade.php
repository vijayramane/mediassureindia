@extends('layouts.app')
@section('title', 'Manage Insurers')
@section('insurers', 'active')
@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Manage Insurers</h3>
        <div class="box-tools">
          <div class="input-group input-group-sm pull-right" style="width:25%">
            <div class="input-group-btn">
              {{--  <a href="{{'insurer/create'}}" class="btn btn-info btn-sm strong"></a>  --}}
              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-create-insurer">
              <i class="fa fa-plus"></i>&nbsp; <b>Create Insurer</b>
              </button>
            </div>
            <input type="text" id="search-insurers" class="form-control pull-right" placeholder="Search">
            {{--  <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>  --}}
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Contact Person</th>
              <th>Contact No</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody id="insurers-table">
            
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
          {{ $insurers->links() }}
        </ul>
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>
<div class="modal fade" id="modal-create-insurer">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Insurer</h4>
      </div>
      <!-- form start -->
      <form id="create-insurer" class="form-horizontal" action="{{'/insurer'}}" method="post">
        <div class="modal-body">
          @include('admin.insurers.insurer')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection