        {{ csrf_field() }}
        <div class="box-body">
          <div class="form-group">
            <label for="inputName" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputName" name="name" value="@yield('name')" placeholder="Insurer Name" required >
            </div>
          </div>
          <div class="form-group">
            <label for="inputAddress" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-10">
              <textarea class="form-control" id="inputAddress" name="address" placeholder="Address" required >@yield('address')</textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="inputContactPerson" class="col-sm-2 control-label">Contact Person</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputContactPerson" name="contact_person" value="@yield('contact_person')" placeholder="Contact Person Name" required >
            </div>
          </div>
          <div class="form-group">
            <label for="inputContactNo" class="col-sm-2 control-label">Contact Number</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputContactNo" name="contact_no" value="@yield('contact_no')" placeholder="Contact Number" required >
            </div>
          </div>
        </div>
      
