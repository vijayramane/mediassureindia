@forelse ($insurers as $insurer)
<tr>
    <td>{{$loop->index+1 + ($insurers->currentPage()-1) * $insurers->perPage()}}</td>
    <td><a href="{{'/insurer/'.$insurer->id}}">{{$insurer->name}}</a></td>
    <td>{{$insurer->contact_person}}</td>
    <td>{{$insurer->contact_no}}</td>
    <td>
        <form class="delete-insurer-{{$insurer->id}}" action="{{'/insurer/'.$insurer->id}}" method="post" >
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" id="delete-insurer-{{$insurer->id}}" class="btn btn-danger btn-xs delete-this"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
        </form>
    </td>
</tr>
@empty
<tr>
    <td colspan="5">No Data Found</td>
</tr>
@endforelse