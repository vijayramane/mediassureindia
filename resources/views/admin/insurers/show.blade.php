@extends('layouts.app')
@section('title', 'Insurer Details')
@section('insurers', 'active')
@section('name', $insurer->name)
@section('address', $insurer->address)
@section('contact_person', $insurer->contact_person)
@section('contact_no', $insurer->contact_no)
@section('content')
<div class="row">
  <div class="col-md-5">
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Insurer Details</h3>
        <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#modal-edit-insurer">
        Edit
        </button>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <dl class="dl-horizontal">
          <dt>Name</dt>
          <dd>@yield('name')</dd>
          <dt>Address</dt>
          <dd>@yield('address')</dd>
          <dt>Contact Person</dt>
          <dd>@yield('contact_person')</dd>
          <dt>Contact No</dt>
          <dd>@yield('contact_no')</dd>
        </dl>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Division</h3>
        <button id="add-division" type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#modal-division">
        Add
        </button>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table class="table table-hover">
              @forelse ($divisions as $division)
                  <tr>
                    <td><a href="#" id="{{ $division->id }}" class="edit-division" data-toggle="modal" data-target="#modal-division" >{{ $division->name }}</a></td>
                    <td class="pull-right">
                      <form class="delete-division-{{$division->id}}" action="{{'/delete_division/'.$division->id}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" id="delete-division-{{$division->id}}" class='btn btn-danger btn-xs delete-this'><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                      </form>
                    </td>
                  </tr>
              @empty
                  <tr><td colspan="2">No Data Found</td></tr>
              @endforelse
          </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Providers</h3>
        <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#modal-add-provider">
        Add
        </button>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table class="table table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Center Name</th>
                  <th>Location</th>
                  <th>State</th>
                  <th>#</th>
                </tr>
              </thead>
              <tbody>
        @forelse ($providers as $provider)
            
                <tr>
                  <td>{{$provider->id}}</td>
                  <td><a href="{{'/provider/'.$provider->id.'/edit'}}" target="_blank">{{$provider->center_name}}</a></td>
                  <td>{{$provider->location}}</td>
                  <td>{{$provider->state}}</td>
                  <td>
                    <form class="remove-provider-{{$provider->id}}" action="{{'/delete_provider/'.$insurer->id.'/'.$provider->id}}" method="post">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <button type="submit" id="remove-provider-{{$provider->id}}" class='btn btn-danger btn-xs delete-this'><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                  </td>
                </tr>
              
        @empty
            <tr><td colspan="5">No Provider Found</td></tr>
        @endforelse
        </tbody>
            </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-7">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Rate Card</h3>
        <button type="button" class="btn btn-info btn-sm pull-right add-rate-card" data-toggle="modal" data-target="#modal-rate-card">
        Add
        </button>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th width="24%">Category</th>
              <th width="40%">Test Details</th>
              <th width="14%">Insurer Cost</th>
              <th width="14%">DC Cost</th>
              <th width="8%">#</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($packages as $package)
            <tr>
              <input type="hidden" id="rate-card-id-{{$loop->index+1}}" value="{{$package->id}}" />
              <td><a href="#" class="edit-rate-card {{$loop->index+1}}" id="category-{{$loop->index+1}}" data-toggle="modal" data-target="#modal-rate-card" >{{$package->category}}</a></td>
              <td id="test-details-{{$loop->index+1}}">{{$package->test_details}}</td>
              <td id="base-rate-{{$loop->index+1}}">{{$package->base_rate}}</td>
              <td id="our-rate-{{$loop->index+1}}">{{$package->our_rate}}</td>
              <td>
                <form class="delete-Package-{{$package->id}}" action="{{'/delete_package/'.$package->id.'/'.$insurer->id}}" method="post" >
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" id="delete-Package-{{$package->id}}" class="btn btn-danger btn-xs delete-this"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                </form>
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="5">No Rate Card Found</td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<div class="modal fade" id="modal-edit-insurer">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Insurer</h4>
      </div>
      <form class="form-horizontal" action="{{'/insurer/'.$insurer->id}}" method="post">
        <div class="modal-body">
          {{ method_field('PUT') }}

          @include('admin.insurers.insurer')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-rate-card">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Rate Card</h4>
      </div>
      <form id="rate-card-form" class="form-horizontal" action="" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="insurer_id" value="{{$insurer->id}}" />
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="inputCategory" class="col-sm-2 control-label">Category</label>
              <div class="col-sm-10">
                <input type="text" class="form-control rate-card-input" id="inputCategory" name="category" placeholder="Category" required >
              </div>
            </div>
            <div class="form-group">
              <label for="inputTestDetails" class="col-sm-2 control-label">Test Details</label>
              <div class="col-sm-10">
                <textarea class="form-control rate-card-input" id="inputTestDetails" name="test_details" placeholder="Test Details" required ></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="inputBaseRate" class="col-sm-2 control-label">Base Rate</label>
              <div class="col-sm-10">
                <input type="text" class="form-control rate-card-input" id="inputBaseRate" name="base_rate" placeholder="Base Rate" required >
              </div>
            </div>
            <div class="form-group">
              <label for="inputOurRate" class="col-sm-2 control-label">Our Rate</label>
              <div class="col-sm-10">
                <input type="text" class="form-control rate-card-input" id="inputOurRate" name="our_rate" placeholder="Our Rate" required >
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-add-provider">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="btn-group pull-right">
        <a href="{{'/provider/create'}}" class="btn btn-warning btn-xs" target="_blank" >Create New Provider</a>&nbsp;
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        </div>
        <h4 class="modal-title">Search Provider</h4>
      </div>
      <div class="modal-body">
        <input type="text" id="search-provider" class="form-control" placeholder="Search Provider" />
        <input type="hidden" id="insurer-id" value="{{$insurer->id}}" />

        <table class="table table-hover">
          <thead>
            <tr>
              <th>ID</th>
              <th>Center Name</th>
              <th>Location</th>
              <th>State</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody id="providers-list">
            
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-division">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Division</h4>
      </div>
      <form id="division-form" class="form-horizontal" action="" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="insurer_id" value="{{$insurer->id}}" />
        <div class="modal-body">
          <div class="box-body">

            <div class="form-group">
              <label for="inputDivision" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control division-input" id="inputDivision" name="division" placeholder="Division" required >
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection