@extends('layouts.app')
@section('title', 'Users Details')
@section('pageHeader', 'User Details')
@section('pageSubHeader', $userDetails->name)
@section('content')
<div class="row">
  <div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="https://placeimg.com/200/200/people" alt="User profile picture">
        <h3 class="profile-username text-center">{{$userDetails->name}}</h3>
        <p class="text-muted text-center">Software Engineer</p>
        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Followers</b> <a class="pull-right">1,322</a>
          </li>
          <li class="list-group-item">
            <b>Following</b> <a class="pull-right">543</a>
          </li>
          <li class="list-group-item">
            <b>Friends</b> <a class="pull-right">13,287</a>
          </li>
        </ul>
        <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    
  </div>
  <!-- /.col -->
  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        {{--  <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>  --}}
        {{--  <li><a href="#timeline" data-toggle="tab">Timeline</a></li>  --}}
        <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
      </ul>
      <div class="tab-content">
        <!-- /.tab-pane -->
        <div class="active tab-pane" id="settings">
          <form class="form-horizontal" method="post" action="{{ '/user/'.$userDetails->id }}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
              <label for="inputName" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="name" class="form-control" id="inputName" name="name" placeholder="Name" value="{{$userDetails->name}}" required />
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email" value="{{$userDetails->email}}" required readonly />
              </div>
            </div>
            <div class="form-group">
              <label for="inputExperience" class="col-sm-2 control-label">Experience</label>
              <div class="col-sm-10">
                <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="inputSkills" class="col-sm-2 control-label">Skills</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
              </div>
            </div>
            <div class="form-group">
              <label for="inputRole" class="col-sm-2 control-label">Skills</label>
              <div class="col-sm-10">
                <select class="form-control required" name="role" id="inputRole">
                  <option>Select Role</option>
                  @foreach($roles as $role)
                    <option value="{{$role->id}}" {{ in_array($role->id, $userDetails->roles->pluck('id')->toArray()) ? "selected":"" }}>{{$role->display_name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Save</button>
                <a href="{{'/user'}}" class="btn btn-default">Cancel</a>
              </div>
            </div>
          </form>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
@endsection