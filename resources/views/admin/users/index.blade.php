@extends('layouts.app')
@section('title', 'Users')
@section('users', 'active')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">User List</h3>
        <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#modal-addUser">
        Add User
        </button>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
          <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Created At</th>
            <th>#</th>
          </tr>
          @forelse($users as $user)
          <tr>
            <td>{{$loop->index+1 + ($users->currentPage()-1) * $users->perPage()}}</td>
            <td><a href="user/{{$user->id}}">{{$user->name}}</a></td>
            <td>{{$user->email}}</td>
            <td>{{ preg_replace('/\[|\]|\"+/', '', preg_replace('/\",\"+/', ', ', $user->roles->pluck('display_name'))) }}</td>
            <td>{{$user->created_at}}</td>
            <td>
              <form class="delete-user-{{$user->id}}" action="{{'/user/'.$user->id}}" method="post" >
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" id="delete-user-{{$user->id}}" class="btn btn-danger btn-xs delete-this"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
              </form>
            </td>
          </tr>
          @empty
            <tr>
              <td colspan="6">No Data Found</td>
            </tr>
          @endforelse
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
          {{ $users->links() }}
        </ul>
      </div>
    </div>
    
    <div class="modal fade" id="modal-addUser">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">New User</h4>
          </div>
          <div class="modal-body">
            
            <form id="createUser" action="{{ '/user' }}" method="post">
              {{ csrf_field() }}
              <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Full name" name="name" value="{{ old('name') }}" required autofocus>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required >
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" id="password" name="password" required >
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation" required>
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <select class="form-control required" name="role">
                  <option value="">Select Role</option>
                  @foreach($roles as $role)
                  <option value="{{$role->id}}">{{$role->display_name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="row">
                <!-- /.col -->
                <div class="col-xs-4">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                </div>
                <div class="col-xs-2">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
                <!-- /.col -->
              </div>
            </form>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    @endsection