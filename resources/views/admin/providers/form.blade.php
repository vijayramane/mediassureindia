<div class="box-body">
  <div class="form-group">
    <label for="inputCenterName" class="col-sm-2 control-label">Center Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputCenterName" name="provider[center_name]" value="{{isset($provider->center_name)?$provider->center_name:''}}" placeholder="Center Name" required >
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
      <textarea class="form-control" id="inputAddress" name="provider[address]" placeholder="Address" required >{{isset($provider->address)?$provider->address:''}}</textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="inputLandmark" class="col-sm-2 control-label">LandMark</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputLandmark" name="provider[landmark]" value="{{isset($provider->landmark)?$provider->landmark:''}}" placeholder="LandMark" >
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputLocation" class="col-sm-4 control-label">Location</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputLocation" name="provider[location]" value="{{isset($provider->location)?$provider->location:''}}" placeholder="Location" required >
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputCity" class="col-sm-4 control-label">City</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputCity" name="provider[city]" value="{{isset($provider->city)?$provider->city:''}}" placeholder="City" required >
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputState" class="col-sm-4 control-label">State</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputState" name="provider[state]" value="{{isset($provider->state)?$provider->state:''}}" placeholder="State" required >
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputPincode" class="col-sm-4 control-label">Pincode</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputPincode" name="provider[pincode]" value="{{isset($provider->pincode)?$provider->pincode:''}}" placeholder="Pincode" >
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputTelNo" class="col-sm-4 control-label">Telephone</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputTelNo" name="provider[tel_no]" value="{{isset($provider->tel_no)?$provider->tel_no:''}}" placeholder="Telephone no with STD Code" >
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputMobile" class="col-sm-4 control-label">Mobile</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputMobile" name="provider[mobile_no]" value="{{isset($provider->mobile_no)?$provider->mobile_no:''}}" placeholder="Mobile" >
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputEmail" class="col-sm-4 control-label">Email</label>
        <div class="col-sm-8">
          <input type="email" class="form-control" id="inputEmail" name="provider[email]" value="{{isset($provider->email)?$provider->email:''}}" placeholder="Email" required >
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputFaxNo" class="col-sm-4 control-label">Fax Number</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputFaxNo" name="provider[fax_no]" value="{{isset($provider->fax_no)?$provider->fax_no:''}}" placeholder="Fax Number with STD Code" >
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputWorkingHours" class="col-sm-4 control-label">Working Hours</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputWorkingHours" name="provider[working_hours]" value="{{isset($provider->working_hours)?$provider->working_hours:''}}" placeholder="Working Hours" >
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputSundayHours" class="col-sm-4 control-label">Sunday Hours</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputSundayHours" name="provider[sunday_hours]" value="{{isset($provider->sunday_hours)?$provider->sunday_hours:''}}" placeholder="Sunday Hours" >
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputRegistrationNo" class="col-sm-4 control-label">Registration No</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputRegistrationNo" name="provider[registration_no]" value="{{isset($provider->registration_no)?$provider->registration_no:''}}" placeholder="Center Registration No" >
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="inputPanNo" class="col-sm-4 control-label">Pan No</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputPanNo" name="provider[pan_no]" value="{{isset($provider->pan_no)?$provider->pan_no:''}}" placeholder="Pan No" >
        </div>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="inputDcOwner" class="col-sm-2 control-label">DC Owner</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputDcOwner" name="provider[dc_owner]" value="{{isset($provider->dc_owner)?$provider->dc_owner:''}}" placeholder="DC Owner" >
    </div>
  </div>
  <div class="form-group">
    <label for="inputContactPerson1" class="col-sm-2 control-label">Contact Person 1</label>
    <div class="col-sm-10"><textarea class="form-control" id="inputContactPerson1" name="provider[contact_person1]" placeholder="Contact Person 1 Details Like Full Name, Mobile No" >{{isset($provider->contact_person1)?$provider->contact_person1:''}}</textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="inputContactPerson2" class="col-sm-2 control-label">Contact Person 2</label>
    <div class="col-sm-10"><textarea class="form-control" id="inputContactPerson2" name="provider[contact_person2]" placeholder="Contact Person 2 Details Like Full Name, Mobile No" >{{isset($provider->contact_person2)?$provider->contact_person2:''}}</textarea>
    </div>
  </div>
</div>
<!-- /.box-body -->