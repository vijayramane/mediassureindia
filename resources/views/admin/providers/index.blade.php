@extends('layouts.app')
@section('title', 'Manage Providers')
@section('providers', 'active')
@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Manage Providers</h3>
        <div class="box-tools">
          <div class="input-group input-group-sm pull-right" style="width:25%">
            <div class="input-group-btn">
              <a href="{{'provider/create'}}" class="btn btn-info btn-sm strong"><i class="fa fa-plus"></i>&nbsp; <b>Add</b></a>
            </div>
            <input type="text" id="search-providers" class="form-control pull-right" placeholder="Search">            
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Center Name</th>
              <th>Location</th>
              <th>City</th>
              <th>Tel No</th>
              <th>Mobile No</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody id="providers-table">
            
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
          {{ $providers->links() }}
        </ul>
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>
@endsection