@extends('layouts.app')
@section('title', 'Create Provider')
@section('providers', 'active')
@section('content')
<div class="row">
  <div class="col-md-8">
    <!-- Horizontal Form -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Update Provider</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form id="add_provider" class="form-horizontal" action="{{'/provider/'.$provider->id}}" method="post">
        
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        @include('admin.providers.form')
        
        <div class="box-footer">
          <div class="btn-group pull-right">
            <button type="submit" class="btn btn-info">Update</button>
            <a href="{{'/provider'}}" class="btn btn-default">Back</a>
          </div>
        </div>
        <!-- /.box-footer -->
      </form>
    </div>

    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Insurers</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Contact Person</th>
              <th>Mobile Number</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($provider->insurers as $insurer)
              <tr>
                <td>{{$insurer->id}}</td>
                <td><a href="{{'/insurer/'.$insurer->id}}" target="_blank">{{$insurer->name}}</a></td>
                <td>{{$insurer->contact_person}}</td>
                <td>{{$insurer->contact_no}}</td>
                <td>
                  <form class="remove-insurer-{{$insurer->id}}" action="{{'/delete_insurer/'.$provider->id.'/'.$insurer->id}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" id="remove-insurer-{{$insurer->id}}" class='btn btn-danger btn-xs delete-this'><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                  </form>
                </td>
              </tr>
            @empty
              <tr>
                <td>No Data Found</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
            <tr>
              <form action="/add_insurer" method="post">
                {{csrf_field()}}
                <input type="hidden" name="provider_id" value="{{$provider->id}}">
                <td colspan="3">
                  <select name="insurer_id" class='form-control required'>
                    @forelse ($all_insurers as $all_insurer)
                        <option value="{{$all_insurer->id}}">{{$all_insurer->name}}</option>
                    @empty
                        <option value="">No Data Found</option>
                    @endforelse
                  </select>
                </td>
                <td><button class='btn btn-warning'>Add</button></td>
              </form>
            </tr>
          </tfoot>
        </table>    
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

  </div>
  <!-- /.box -->
  <div class="col-md-4">
    <!-- Horizontal Form -->
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Facilities</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-striped">
          <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
          </tr>
          @include('admin.providers.facilities')
        </table>
      </div>
      
    </div>
    @endsection