@extends('layouts.app')
@section('title', 'Create Provider')
@section('providers', 'active')
@section('content')
<div class="row">
  <div class="col-md-8">
    <!-- Horizontal Form -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Create Provider</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form id="add_provider" class="form-horizontal" action="{{'/provider'}}" method="post">
        
        {{ csrf_field() }}
        @include('admin.providers.form')
        
        <div class="box-footer">
          <div class="btn-group pull-right">
            <button type="submit" class="btn btn-info">Save</button>
            <a href="{{'/provider'}}" class="btn btn-default">Cancel</a>
          </div>
        </div>
        <!-- /.box-footer -->
      </form>
    </div>
  </div>
  <!-- /.box -->
  <div class="col-md-4">
    <!-- Horizontal Form -->
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Facilities</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-striped">
          <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
          </tr>
          @include('admin.providers.facilities')
        </table>
      </div>
      
    </div>
    @endsection