@foreach($facilities as $facility)
    <tr>
        <td><input form="add_provider" type="checkbox" name="facilities[]" value="{{$facility->id}}" {{(isset($available_facilities) && $available_facilities->search($facility->id) !== false) ?'checked':''}} /></td>
        <td>{{$facility->name}}</td>
    </tr>
@endforeach
