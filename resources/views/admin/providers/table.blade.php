@forelse ($providers as $provider)
<tr>
    <td>{{$provider->id}}</td>
    <td><a href="{{'/provider/'.$provider->id}}">{{$provider->center_name}}</a></td>
    <td>{{$provider->location}}</td>
    <td>{{$provider->city}}</td>
    <td>{{$provider->tel_no}}</td>
    <td>{{$provider->mobile_no}}</td>
    <td>
        <form class="delete-provider-{{$provider->id}}" action="{{'/provider/'.$provider->id}}" method="post" >
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" id="delete-provider-{{$provider->id}}" class="btn btn-danger btn-xs delete-this"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
        </form>
    </td>
</tr>
@empty
<tr>
    <td colspan="5">No Data Found</td>
</tr>
@endforelse