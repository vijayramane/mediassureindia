@extends('layouts.app')
@section('title', 'Call Statuses')
@section('call-status', 'active')
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Call Statuses</h3>
                <button type="button" id="add-call-status" class="btn btn-info pull-right" data-toggle="modal" data-target="#modal-add-edit-call-status"> Add </button>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($call_statuses as $call_status)
                        <tr>
                            <td>{{$loop->index+1 + ($call_statuses->currentPage()-1) * $call_statuses->perPage()}}</td>
                            <td><a href="#" class="edit-call-status" id="{{$call_status->id}}" data-toggle="modal" data-target="#modal-add-edit-call-status">{{$call_status->name}}</a></td>
                            <td>
                                <form class="delete-call-status-{{$call_status->id}}" action="{{'/call-status/'.$call_status->id}}" method="post" >
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" id="delete-call-status-{{$call_status->id}}" class="btn btn-danger btn-xs delete-this"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                {{$call_statuses->links()}}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-add-edit-call-status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="call-status-modal-title"></h4>
            </div>
            <form id="callStatusAddEdit" class="form-horizontal" method="post" action="{{'/call-status'}}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="callStatusName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="callStatusName" name="name" placeholder="Name" required />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection