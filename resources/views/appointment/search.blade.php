<div class="row">
	<div class="col-md-12">
		<div class="box box-warning box-solid collapsed-box">
			<div class="box-header with-border">
				<h3 class="box-title">Search Panel</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-plus"></i>
					</button>
					<!-- <button type="button" class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i>
					</button> -->
				</div>
				<!-- /.box-tools -->
			</div>
			<!-- /.box-header -->
			<div class="box-body" style="">
				<form class="form-horizontal">

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="inputDateType" class="col-sm-4 control-label">Date Type</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="inputDateType" placeholder="Date Type">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="inputDateFrom" class="col-sm-4 control-label">Date From</label>
								<div class="col-sm-8">
									<input type="date" class="form-control" id="inputDateFrom" placeholder="Date From">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="inputDateTo" class="col-sm-4 control-label">Date To</label>
								<div class="col-sm-8">
									<input type="date" class="form-control" id="inputDateTo" placeholder="Date To">
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="inputStatus" class="col-sm-4 control-label">Status</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="inputStatus" placeholder="Status">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="inputAppointmentType" class="col-sm-4 control-label">Appointment Type</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="inputAppointmentType" placeholder="Appointment Type">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="inputCorporate" class="col-sm-4 control-label">Corporate</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="inputCorporate" placeholder="Corporate">
								</div>
							</div>
						</div>
					</div>

					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-default">Clear</button>
						<button type="submit" class="btn btn-info pull-right">Search</button>
					</div>
					<!-- /.box-footer -->
				</form>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>