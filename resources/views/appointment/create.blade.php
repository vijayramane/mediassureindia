
@extends('layouts.app')
@section('title', 'Create Appointment')
@section('acreate', 'active')

@section('content')

<div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Create Appointment</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" action="{{ '/appointment' }}" method="post">
        
        {{ csrf_field() }}

        @include('appointment.form')

        <input type="hidden" name="appointment[status]" value="New">
        
        <div class="box-footer">
          {{--  <div class="btn-group">  --}}
          <div class="col-md-3 col-offset-3"></div>
          <div class="col-md-3">
            <button type="submit" class="btn btn-block btn-info">Save</button>
            </div>
            <div class="col-md-3">
            <a href="{{ '/appointment' }}" class="btn btn-block btn-default">Cancel</a>
            </div>
          {{--  </div>  --}}
        </div>
        <!-- /.box-footer -->
      </form>
    </div>
  </div>
    
@endsection