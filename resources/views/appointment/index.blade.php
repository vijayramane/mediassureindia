@extends('layouts.app') @section('title', 'Active Appointment') @section('aactive', 'active') @section('content')
@include('appointment.search')

<div class="row">
  <div class="col-xs-12">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Active Appointments</h3>
        <div class="box-tools">
          <div class="input-group input-group-sm pull-right" style="width:25%">
            <div class="input-group-btn">
              <a href="{{ '/appointment/create' }}" class="btn btn-info btn-sm strong">
                <i class="fa fa-plus"></i>&nbsp;
                <b>Create</b>
              </a>
            </div>
            <input type="text" id="search-providers" class="form-control pull-right" placeholder="Search">
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>RequestID</th>
              <th>ProposalNo</th>
              <th>Proposal</th>
              <th>Insurer</th>
              <th>Insurer Div</th>
              <th>Provider</th>
              <th>Tests</th>
              <th>Appt Date</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @forelse ($appointments as $appointment)
            <tr>
              <td>
                <a href="{{ '/appointment/'.$appointment->id }}" target="_blank">{{ 'MEDI-'. $appointment->id }}</a>
              </td>
              <td>{{ $appointment->application_no }}</td>
              <td>{{ $appointment->customer['name'] }}</td>
              <td>{{ $appointment->insurer['insurer']['name'] }}</td>
              <td>{{ $appointment->insurer['division']['name'] }}</td>
              <td>{{ $appointment->provider['center_name'] }}</td>
              <td>{{ $appointment->packages->implode('category', ', ') }}</td>
              <td id="date-time-{{$appointment->id}}">{{ $appointment->date }}, {{ $appointment->time }}</td>
              <td>{{ $appointment_statuses[$appointment->status_id] }}</td>
              <td>
                <!-- <i class="fa fa-fw fa-cogs"></i> -->

                <div class="dropdown">
                  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-fw fa-cog"></i>
                    <span class="fa fa-caret-down"></span>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right">

                    <li>
                      <a href="#">
                        <i class="fa fa-fw fa-eye"></i>View</a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-fw fa-commenting"></i>Communication</a>
                    </li>
                    @if (in_array($appointment->status_id, array(1,2)))
                      <li>
                        <a href="#" data-toggle="modal" data-target="#confirm-appointment"
                          onclick="set_appointment_id({{ $appointment->id }})">
                          <i class="fa fa-fw fa-calendar-check-o text-green"></i>Confirmed</a>
                      </li>
                    @endif
                    @if (in_array($appointment->status_id, array(2,3,6,7)))
                    <li>
                      <a href="#" data-toggle="modal" data-target="#rescheduled-appointment"
                        onclick="set_appointment_id({{ $appointment->id }})">
                        <i class="fa fa-fw fa-calendar-times-o"></i>Re-Scheduled Appointment</a>
                    </li>
                    @endif
                    @if (in_array($appointment->status_id, array(3,5)))
                    <li>
                      <a href="#">
                        <i class="fa fa-fw fa-circle-o text-black"></i>No Show</a>
                    </li>
                    @endif
                    @if (in_array($appointment->status_id, array(3)))
                    <li>
                      <a href="#">
                        <i class="fa fa-fw fa-warning text-warning"></i>Report Pending</a>
                    </li>
                    @endif
                    @if (in_array($appointment->status_id, array(5)))
                    <li>
                      <a href="#" data-toggle="modal" data-target="#report-upload">
                        <i class="fa fa-fw fa-cloud-upload text-warning"></i>Report Upload</a>
                    </li>
                    @endif
                    @if (in_array($appointment->status_id, array(3,5)))
                    <li>
                      <a href="#">
                        <i class="fa fa-fw fa-adjust text-dark"></i>Partial Show</a>
                    </li>
                    @endif
                    @if (in_array($appointment->status_id, array(8)))
                    <li>
                      <a href="#" data-toggle="modal" data-target="#Closed">
                        <i class="fa fa-fw fa-check text-green"></i>Closed</a>
                    </li>
                    @endif
                    <li>
                      <a href="#" data-toggle="modal" data-target="#abort"
                        onclick="set_appointment_id({{ $appointment->id }})">
                        <i class="fa fa-fw fa-remove text-red"></i>Abort</a>
                    </li>

                  </ul>
                </div>

              </td>
            </tr>
            @empty
            <tr>
              <td colspan="8">No Data Found</td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
          {{ $appointments->links() }}
        </ul>
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>

<div class="modal fade" id="confirm-appointment">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Confirm Appointment</h4>
      </div>
      <form role="form" action="{{ url('appointment-status') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="Confirm">
        <input type="hidden" name="appointment_id" class="appointment_id">
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group">
                <label for="dateOfVisit">Requested date of visit</label>
                <input type="text" class="form-control" name="date_of_visit" id="date-of-visit"
                  placeholder="Date Of Visit" readonly />
              </div>
            </div>
            <div class="col-md-6 col-xs-12">
              <div class="form-group">
                <label for="contactPersonName">Contact person's name at dc*</label>
                <input type="text" class="form-control" name="log[contact_person_name]" id="contactPersonName"
                  placeholder="Contact Person Name">
              </div>
            </div>
            <div class="col-md-6 col-xs-12">
              <div class="form-group">
                <label for="contactPersonPhone">Contact person's phone number*</label>
                <input type="text" class="form-control" name="log[contact_person_number]" id="contactPersonPhone"
                  placeholder="Contact Person Phone">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputRemark">Remark*</label>
                <textarea class="form-control" name="remark" id="inputRemark" placeholder="Remark"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="col-md-12">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Confirm Appointment</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="cancel-appointment">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Cancel Appointment</h4>
      </div>
      <form role="form" action="{{ url('appointment-status') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="Cancel">
        <input type="hidden" name="appointment_id" class="appointment_id">
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputRemark">Remark*</label>
                <textarea class="form-control" name="remark" id="inputRemark" placeholder="Remark"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="col-md-12">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Cancel Appointment</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="upload-report">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Upload Report</h4>
      </div>
      <form role="form" action="{{ url('upload-report') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="appointment_id" class="appointment_id">
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputRemark">Remark*</label>
                <textarea class="form-control" name="remark" id="inputRemark" placeholder="Remark"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="col-md-12">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Cancel Appointment</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="rescheduled-appointment">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Re-Scheduled Appointment</h4>
      </div>
      <form role="form" action="{{ url('appointment-status') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="Re-Scheduled">
        <input type="hidden" name="appointment_id" class="appointment_id">
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-4">
              <div class="form-group">
                <label for="current-requested-date">Current requested date</label>
                <input type="text" class="form-control" id="current-requested-date" readonly />
              </div>
            </div>
            {{-- <div class="col-md-6"> --}}
            {{-- <label for="reschedule-date">Select reschedule date</label> --}}
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="appointmentDate">Appointment Date</label>
                  <input type="date" class="form-control" id="appointmentDate" name="appointment[date]"
                    value="{{isset($appointment->date)?$appointment->date:''}}" placeholder="Appointment Date">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="appointmentTime">Appointment Time</label>
                  <select name="appointment[time]" class="form-control">
                    @for ($i = 6; $i < 11; $i++) <option>{{ $i }} - {{ ($i+1) }} am</option>
                      <option>{{ $i }}.30 - {{ ($i+1) }}.30 am</option>
                      @endfor
                      <option>11 am - 12 pm</option>
                      <option>11.30 am - 12.30 pm</option>
                      <option>12 - 1 pm</option>
                      <option>12.30 - 1.30 pm</option>
                      @for ($i = 1; $i < 10; $i++) <option>{{ $i }} - {{ ($i+1) }} pm</option>
                        <option>{{ $i }}.30 - {{ ($i+1) }}.30 pm</option>
                        @endfor
                  </select>
                </div>
              </div>
            </div>
            {{-- </div> --}}
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputRemark">Remark*</label>
                <textarea class="form-control" name="remark" id="inputRemark" placeholder="Remark"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="col-md-12">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update Appointment</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection