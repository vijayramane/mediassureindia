
@extends('layouts.app');
@section('title', 'Show Appointment')
@section('acreate', 'active')

@section('content')

    <div class="row">
        <div class="col-md-4">

          <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Patient Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Name</dt>
                <dd>{{ $appointment->customer->name }}</dd>
                <dt>E-Mail</dt>
                <dd>{{ $appointment->customer->email }}</dd>
                <dt>Mobile</dt>
                <dd>{{ $appointment->customer->mobile }}</dd>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Appointment Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Date</dt>
                <dd>{{ $appointment->date }} {{ $appointment->time }}</dd>
                <dt>Provider Name/ Address</dt>
                <dd>fdsf dfdsfds dfdsfds fdf fsdsfdsf dffdsds sds</dd>
                <dt>Primary Contact Name</dt>
                <dd>Satish Shirke</dd>
                <dt>Landline Number</dt>
                <dd>54675476</dd>
                <dt>Primary Mail</dt>
                <dd>vkr0704@gmail.com</dd>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Home Visit Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Name</dt>
                <dd>Mr. Vijay Ramane</dd>
                <dt>Employee Id</dt>
                <dd>1KAF963377</dd>
                <dt>Mail Id</dt>
                <dd>vkr0704@gmail.com</dd>
                <dt>Contact Number</dt>
                <dd>54675476</dd>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Agent Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Name</dt>
                <dd>{{ $appointment->agent->agent_name }}</dd>
                <dt>Agent Code</dt>
                <dd>{{ $appointment->agent->agent_code }}</dd>
                <dt>Mail Id</dt>
                <dd>{{ $appointment->agent->agent_email }}</dd>
                <dt>Contact Number</dt>
                <dd>{{ $appointment->agent->agent_mobile }}</dd>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <div class="col-md-8">
        
      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
          
        <!-- timeline time label -->
        <li class="time-label">
            <span class="bg-green">
                {{ date('d M Y') }}
            </span>
        </li>
        <!-- /.timeline-label -->
    
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-envelope-open bg-blue"></i>
            <div class="timeline-item">
                {{-- <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
    
                <h3 class="timeline-header"><a href="#">Appointment Confirmed</a> ...</h3> --}}
    
                <div class="timeline-body">
                  <form action="{{ url('calling-history') }}" method="POST">
                    {{ @csrf_field() }}
                    <input type="hidden" name="appointment_id" value="{{ $appointment->id }}">
                    <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>Select a receiver</label>
                          <select name="receiver" class="form-control">
                            <option value="Customer">Customer</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Select call status</label>
                          <select name="call_status_id" class="form-control">
                            <option value="-1">Select Call Status</option>
                            @foreach ($call_statuses as $key=>$value)
                              <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Add correspondence notes</label>
                          <textarea name="notes" class="form-control" rows="1" placeholder="Enter ..."></textarea>
                        </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group">
                            <label>#</label>
                            <button class="btn btn-info">Save</button>
                          </div>
                        </div>
                    </div>
                  </form>
                </div>
    
            </div>
        </li>
        <!-- END timeline item -->


          @foreach ($calling_history as $value)
                <!-- timeline time label -->
              <li class="time-label">
                  <span class="bg-blue">
                      {{ $value->updated_at }}
                  </span>
              </li>
              <!-- /.timeline-label -->
              <!-- timeline item -->
              <li>
                  <!-- timeline icon -->
                  <i class="fa fa-envelope bg-blue"></i>
                  <div class="timeline-item">
                      <span class="time"> {{ $value->name }} </span>
                      <h3 class="timeline-header"><a href="#">{{ $call_statuses[$value->call_status_id] }}</a></h3>
                      <div class="timeline-body">
                          {{ $value->notes }}
                      </div>
                      {{-- <div class="timeline-footer">
                          <a class="btn btn-primary btn-xs">MA-1000</a>
                      </div> --}}
                  </div>
              </li>
              <!-- END timeline item -->
          @endforeach

          {{-- @foreach ($appointment_logs as $logs)
                <!-- timeline time label -->
              <li class="time-label">
                  <span class="bg-blue">
                      {{ $logs->created_at }}
                  </span>
              </li>
              <!-- /.timeline-label -->
              <!-- timeline item -->
              <li>
                  <!-- timeline icon -->
                  <i class="fa fa-envelope bg-blue"></i>
                  <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                      <h3 class="timeline-header"><a href="#">{{ $logs->status }}</a> ...</h3>
                      <div class="timeline-body">
                          {{ $logs->comment }}
                      </div>
                      <div class="timeline-footer">
                          <a class="btn btn-primary btn-xs">MA-1000</a>
                      </div>
                  </div>
              </li>
              <!-- END timeline item -->
          @endforeach --}}

            <!-- timeline time label -->
            <li class="time-label">
                <span class="bg-red">
                    10 Feb. 2014
                </span>
            </li>
            <!-- /.timeline-label -->
        
            <!-- timeline item -->
            <li>
                <!-- timeline icon -->
                <i class="fa fa-envelope bg-blue"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
        
                    <h3 class="timeline-header"><a href="#">Appointment Created in System</a> ...</h3>
        
                    {{-- <div class="timeline-body">
                        ...
                        Content goes here
                    </div> --}}
        
                    {{-- <div class="timeline-footer">
                        <a class="btn btn-primary btn-xs">MA-1000</a>
                    </div> --}}
                </div>
            </li>
            <!-- END timeline item -->
        </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</div>
    </div>




@endsection