<div class="box-body">

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="inputInsuranceName">Insurance Name</label>
				<select class="form-control select-insurer" name="customer_insurer[insurer_id]">
					<option>Select Insurer</option>
					@forelse ($insurers as $insurer)
					<option value="{{$insurer->id}}">{{$insurer->name}}</option>
					@empty
					<option>No Data Found</option>
					@endforelse
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="inputDivision">Division</label>
				<select class="form-control" id="select-division" name="customer_insurer[division_id]" disabled>

				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="inputBranch">Branch</label>
				<input type="text" class="form-control" id="inputBranch" name="customer_insurer[branch]" value="{{isset($customer_insurer->branch)?$customer_insurer->branch:''}}"
				 placeholder="Branch Name" />
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="inputCustomerName">Customer</label>
				<input type="text" class="form-control" id="inputCustomerName" name="customer[name]" value="{{isset($customer->name)?$customer->name:''}}"
				 placeholder="Customer Name" >
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label >Gender</label>
				<div class="radio">
					<label>
						<input type="radio" name="customer[gender]" value="Male" checked="checked">Male</label>
					<label>
						<input type="radio" name="customer[gender]" value="Female" >Female</label>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="inputAge">Age</label>
				<input type="text" class="form-control" id="inputAge" name="customer[age]" value="{{isset($customer->age)?$customer->age:''}}"
				 placeholder="Age">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputAnnualPremium">Annual Premium</label>
				<input type="text" class="form-control" id="inputAnnualPremium" name="customer[annual_premium]" value="{{isset($customer->annual_premium)?$customer->annual_premium:''}}"
				 placeholder="Annual Premium">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputAddress">Address</label>
				<input type="text" class="form-control" id="inputAddress" name="customer[address]" value="{{isset($customer->address)?$customer->address:''}}"
				 placeholder="Address" >
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputState">State</label>
				<input type="text" class="form-control" id="inputState" name="customer[state]" value="{{isset($customer->state)?$customer->state:''}}"
				 placeholder="State" >
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputCity">City</label>
				<input type="text" class="form-control" id="inputCity" name="customer[city]" value="{{isset($customer->city)?$customer->city:''}}"
				 placeholder="City">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputPinCode">Pin Code</label>
				<input type="text" class="form-control" id="inputPinCode" name="customer[pincode]" value="{{isset($customer->pincode)?$customer->pincode:''}}"
				 placeholder="Pin Code">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputEmail">Email</label>
				<input type="text" class="form-control" id="inputEmail" name="customer[email]" value="{{isset($customer->email)?$customer->email:''}}"
				 placeholder="Email" >
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputMobile">Mobile</label>
				<input type="text" class="form-control" id="inputMobile" name="customer[mobile]" value="{{isset($customer->mobile)?$customer->mobile:''}}"
				 placeholder="Mobile" >
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputApplicationNo">Application No</label>
				<input type="text" class="form-control" id="inputApplicationNo" name="appointment[application_no]" value="{{isset($appointment->application_no)?$appointment->application_no:''}}"
				 placeholder="Application No">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputPolicyNo">Policy No</label>
				<input type="text" class="form-control" id="inputPolicyNo" name="appointment[policy_no]" value="{{isset($appointment->policy_no)?$appointment->policy_no:''}}"
				 placeholder="Policy Number">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputAgentName">Agent Name</label>
				<input type="text" class="form-control" id="inputAgentName" name="agent[agent_name]" value="{{isset($agent->agent_name)?$agent->agent_name:''}}"
				 placeholder="Agent Name" >
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputAgentCode">Agent Code</label>
				<input type="text" class="form-control" id="inputAgentCode" name="agent[agent_code]" value="{{isset($agent->agent_code)?$agent->agent_code:''}}"
				 placeholder="Agent Code" >
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputAgentEmail">Agent Email</label>
				<input type="text" class="form-control" id="inputAgentEmail" name="agent[agent_email]" value="{{isset($agent->agent_email)?$agent->agent_email:''}}"
				 placeholder="Agent Email">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="inputAgentMobile">Agent Mobile</label>
				<input type="text" class="form-control" id="inputAgentMobile" name="agent[agent_mobile]" value="{{isset($agent->agent_mobile)?$agent->agent_mobile:''}}"
				 placeholder="Agent Mobile">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="inputSumAssured">Sum Assured</label>
				<input type="text" class="form-control" id="inputSumAssured" name="appointment[sum_assured]" value="{{isset($appointment->sum_assured)?$appointment->sum_assured:''}}"
				 placeholder="Sum Assured" >
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label >Paid/Sponsored</label>
				<div class="radio">
					<label>
						<input type="radio" name="appointment[paid_sponsored]" value="Paid" checked="checked">Paid</label>
					<label>
						<input type="radio" name="appointment[paid_sponsored]" value="Sponsored">Sponsored</label>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label >Group/Individual</label>
				<div class="radio">
					<label>
						<input type="radio" name="appointment[group_individual]" value="Group">Group</label>
					<label>
						<input type="radio" name="appointment[group_individual]" value="Individual" checked="checked">Individual</label>
				</div>
			</div>
		</div>
	</div>

	<div class="box box-warning">
		<div class="box-header with-border">
			<h3 class="box-title">Select Appropriate Packages</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">
					<label for="">Test Master</label>
					<select multiple="multiple" class="form-control" id="test-master" />

					</select>
				</div>
				<div class="col-md-1 text-center">
					<i style="padding-top: 80%;" class="fa fa-arrow-right" aria-hidden="true"></i>
					<br>
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="col-md-3">
					<label for="">Test to be Done</label>
					<select multiple="multiple" class="form-control" id="test-to-be-done" name="packages[]">

					</select>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-3">
					<div class="form-group">
						<label >Visit Type</label>
						<div class="radio">
							<label>
								<input type="radio" name="appointment[visit_type]" value="Home Visit">Home Visit</label>
							<label>
								<input type="radio" name="appointment[visit_type]" value="Center Visit" checked="checked">Center Visit</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">DC Selection</h3>
		</div>
		<div class="box-body">
			<div class="form-group">

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="appointmentDate">Appointment Date</label>
							<input type="date" class="form-control" id="appointmentDate" name="appointment[date]" value="{{isset($appointment->date)?$appointment->date:''}}"
							 placeholder="Appointment Date">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="appointmentTime">Appointment Time</label>
							<select name="appointment[time]" class="form-control">
								@for ($i = 6; $i < 11; $i++)
									<option>{{ $i }} - {{ ($i+1) }} am</option>
									<option>{{ $i }}.30 - {{ ($i+1) }}.30 am</option>
								@endfor
								<option>11 am - 12 pm</option>
								<option>11.30 am - 12.30 pm</option>
								<option>12 - 1 pm</option>
								<option>12.30 - 1.30 pm</option>
								@for ($i = 1; $i < 10; $i++)
									<option>{{ $i }} - {{ ($i+1) }} pm</option>
									<option>{{ $i }}.30 - {{ ($i+1) }}.30 pm</option>
								@endfor
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="select-state">Select State</label>
							<select class="form-control" id="select-state" name="state" >

							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="select-city">Select City</label>
							<select class="form-control" id="select-city" name="city" >

							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="select-area">Select Area</label>
							<select class="form-control" id="select-area" name="area" >

							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="select-providers">Daignostic Center</label>
							<select class="form-control" id="select-providers" name="provider_id">

							</select>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>
<!-- /.box-body -->