let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
.sass('resources/assets/sass/app.scss', 'public/css')
.styles([
 'resources/assets/plugins/font-awesome/css/font-awesome.min.css',
 'resources/assets/plugins/Ionicons/css/ionicons.min.css',
 'resources/assets/plugins/bootstrap-toastr/toastr.min.css',
 'resources/assets/plugins/multi-select/multi-select.css',
 'resources/assets/css/AdminLTE.min.css',
 'resources/assets/css/skins/skin-blue.min.css',
 'resources/assets/css/custom.css',
], 'public/css/all.css')
.scripts([
 'resources/assets/plugins/bootstrap-toastr/toastr.min.js',
 'resources/assets/plugins/multi-select/jquery.multi-select.js',
 'resources/assets/js/ui-toastr.js',
 'resources/assets/js/jquery.validate.min.js',
 'resources/assets/js/validate.js',
 'resources/assets/js/adminlte.min.js',
 'resources/assets/js/custom.js'
], 'public/js/all.js')
.version();