<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('user', 'UserController');
    Route::resource('provider', 'ProviderController');
    Route::resource('insurer', 'InsurerController');

    Route::resource('call-status', 'CallStatusController');
    Route::resource('appointment-status', 'AppointmentStatusController');

    Route::resource('appointment', 'AppointmentController');

    Route::post('calling-history', 'AppointmentController@storeCallingHistory');

    Route::resource('facility', 'FacilityController', ['only' => [
        'index', 'store', 'update', 'destroy'
        ]]);
        
        Route::post('/insurer/store_package', 'InsurerController@storePackage');
        Route::put('/insurer/update_package/{package_id}', 'InsurerController@updatePackage');
        Route::delete('/delete_package/{package_id}/{insurer_id}', 'InsurerController@deletePackage');
        
        Route::post('/search_provider', 'InsurerController@searchProvider');
        Route::post('/add_provider', 'InsurerController@addProvider');
        Route::delete('/delete_provider/{insurer_id}/{provider_id}', 'InsurerController@deleteProvider');
        
        Route::post('/store_division', 'InsurerController@storeDivision');
        Route::put('/update_division/{division_id}', 'InsurerController@updateDivision');
        Route::delete('/delete_division/{division_id}', 'InsurerController@deleteDivision');
        
        Route::post('/search_insurers', 'InsurerController@searchInsurers');
        Route::post('/search_providers', 'ProviderController@searchProviders');

        Route::post('/add_insurer', 'ProviderController@addInsurer');
        Route::delete('/delete_insurer/{provider_id}/{insurer_id}', 'ProviderController@deleteInsurer');
        
        Route::get('/get_division', 'AppointmentController@getDivision');
        Route::get('/get_packages', 'AppointmentController@getPackages');
        Route::get('/get_insurer_providers_state', 'AppointmentController@getInsurerProvidersState');
        Route::get('/get_city', 'AppointmentController@getCity');
        Route::get('/get_area', 'AppointmentController@getArea');
        Route::get('/get_providers', 'AppointmentController@getProviders');
               
        // Route::post('/appointment-status', 'AppointmentController@change_appointment_status');

    });
    